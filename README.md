# Things to Try

1. Make the spaceship's gun shoot faster
	1. select the "Player" object in the Hierarchy view
	2. look around in the Inspector view until you find "Player Controller (Script."
	3. mess around with the Fire Rate variable to see how it affects the gun

2. Now that your gun shots faster, lets make the game harder by spawning more asteroids
	1. select the "Game Controller" in the Hierarchy view
	2. look around in the Inspector view until you find "Game Controller (Script."
	3. try increasing Hazard Count and decreasing Spawn Wait
		- don't make Spawn Wait too small though or else the asteroids will spawn on top of each other and immediately explode

3. Make the asteroids move faster
	1. expand the Prefabs directory in the Project view
	2. select Asteroid
	3. look around in the Inspector view until you find "Mover (Script."
	4. modify the Speed variable
		- make sure speed is negative to make the asteroid move down
	5. with the Asteroid still selected, try tweaking the following the variables to see what happens:
		- Score Value
		- Tumble
		
4. Replace the player ship with a model of an enemy ship
	1. select the "Player" object in the Hierarchy view
	2. look around in the Inspector view until you find "Vehicle_player Ship (Mesh Filter."
	3. click on the small circle with the dot in the middle which is to the right of the 
		text box saying "vehicle_playerShip"
	4. select "vehicle_enemyShip" in the pop-up
	5. this will replace the model with the enemy ship model but the texture is wrong now
	6. use the same technique described above to change the textures from the vehicle_playerShip 
		textures to the vehicle_enemyShip textures
	
5. Now that you are learning your way around Unity, try the following tasks:
	- change the color of the "Game Over!" text
		- hint: find the "GameOverText" object in the Hierarchy view 
	- change the character used to restart from 'R' to 'Q'
		- hint: you will need to edit the GameController script
	- limit how far up the player's spaceship can go
		- hint: look at the Boundary settings on the "Player Controller (Script."
			on the "Player" object
	- move the "Game Over!" text so that it is at the top of the play area
		- hint: you will need to use the Scene view

6. Coding challenge - make it so the game keeps track of the high score
	- hint: you will need to edit the GameController script
	- have the score in the upper left be green when it is the new high score
